package exercise3;

public class Customer {
    public static void main(String[] args) {
        Details person1 = new Details("Rohit", "Lucknow", 324324324234l);
        System.out.println(person1);
    }
}

class Details {
    private String name;
    private String Address;
    private long contact;

    public Details(String name, String address, long contact) {
        this.name = name;
        Address = address;
        this.contact = contact;
    }
}