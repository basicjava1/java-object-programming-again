package exercise6;

public abstract class Cooking {
    public void execute(){
        getReady();
        doTheDish();
        cleanup();
    }

    abstract void getReady();
    abstract void doTheDish();
    abstract void cleanup();
}
