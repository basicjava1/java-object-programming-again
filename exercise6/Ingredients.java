package exercise6;

public class Ingredients extends Cooking{
    @Override
    void getReady() {
        System.out.println("Turning ON stove");
    }

    @Override
    void doTheDish() {
        System.out.println("Cutting Vegetables");
    }

    @Override
    void cleanup() {
        System.out.println("Cleaning the shelf");
    }
}
