package exercise4;

import java.util.ArrayList;

public class Novels {
    public static void main(String[] args) {
        Contents novel1 = new Contents(0000, "The Habit of Winning","Prakash");
        novel1.addReview(new Review(1122, "Best Selling Novel",4));
        System.out.println(novel1);
    }
}


class Contents{
    private int id;
    private String name;
    private String author;
    private ArrayList<Review> reviews = new ArrayList<Review>();

    public Contents(int id, String name, String author) {
        this.id = id;
        this.name = name;
        this.author = author;
    }


    public void addReview(Review review) {
        this.reviews.add(review);
    }

    @Override
    public String toString() {
        return "Contents{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", reviews=" + reviews +
                '}';
    }
}
