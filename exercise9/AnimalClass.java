package exercise9;


    abstract class Animal {
        abstract void bark();
    }

    class Dog extends Animal {
        public void bark(){
            System.out.println("Bark");
        }
    }

    class Cat extends Animal {
        public void bark(){
            System.out.println("Meow");
        }
    }
public class AnimalClass {
    public static void main(String[] args) {
        Animal[] animals = {new Cat(), new Dog()};
        for(Animal animal : animals){
            animal.bark();
        }
    }

}
