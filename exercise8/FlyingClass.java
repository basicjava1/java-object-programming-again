package exercise8;

interface Flyable{
    void fly();
}
class Bird implements Flyable {
    public void fly(){
        System.out.println("Fly with wings");
    }
}

class Aeroplane implements Flyable{
    public void fly(){
        System.out.println("Fly with fuel");
    }
}
public class FlyingClass{
    public static void main(String[] args) {
        Flyable[] flyingObject = {new Bird(), new Aeroplane()};
        for(Flyable object : flyingObject){
            object.fly();
        }
    }
}
