package exercise1;

public class Fan {
    public static void main(String[] args) {
        Methods bajajFan = new Methods("Aman",3.14,"Black");
        System.out.println(bajajFan);
        bajajFan.switchOn();
        System.out.println(bajajFan);
        bajajFan.changeSpeed((byte) 3);
    }
}

class Methods{
    private String make;
    private double radius;
    private String color;
    private boolean isOn;
    private byte speed;

    public Methods(String make, double radius, String color) {
        this.make = make;
        this.radius = radius;
        this.color = color;
    }

    void switchOn(){
        boolean isOn = true;
        changeSpeed((byte)5);
        System.out.println("Fan is ON");
    }

    void switchOff(){
        boolean isOn = false;
        changeSpeed((byte)0);
        System.out.println("Fan is OFF");
    }

    void changeSpeed(byte speed){
        this.speed = speed;
    }

    public String toString(){
        return String.format("make %s, radius %f, color %s,isOn %b, speed %d $",make, radius,color,isOn,speed);
    }
}